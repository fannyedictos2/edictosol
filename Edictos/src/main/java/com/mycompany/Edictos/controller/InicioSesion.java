/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Edictos.controller;

import com.mycompany.Edictos.business.UsuarioBusiness;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.inject.Inject;
import org.omnifaces.util.Ajax;
import org.primefaces.model.menu.BaseMenuModel;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.omega.component.menu.OmegaMenuRenderer;

/**
 *
 * @author Admin
 */
@Named(value = "inicioSesion")
@SessionScoped
public class InicioSesion implements Serializable {

    @Inject
    UsuarioBusiness business;

    String password;
    String nombreUsuario;

    /*Usuario usuario;*/

    boolean nuevo = true;

    private BaseMenuModel model;
    private OmegaMenuRenderer renderer;

    @PostConstruct
    public void init() {
        this.password = "";
        this.nombreUsuario = "";

        this.prepareSideBar();
    }

    public void prepareSideBar() {
        this.model = new BaseMenuModel();
        this.renderer = new OmegaMenuRenderer();

        /*if (usuario == null) {
            return;
        }*/

        DefaultMenuItem menuPrincipal = new DefaultMenuItem("Menú Principal");
        menuPrincipal.setStyleClass("iconcasa");
        menuPrincipal.setTitle("Inicio");
        menuPrincipal.setUrl("index.xhtml?faces-redirect=true");
        menuPrincipal.setUpdate("@form");

        model.addElement(menuPrincipal);

        DefaultMenuItem salir = new DefaultMenuItem("Salir");

        salir.setStyleClass("iconsidebarsalir outter");
        salir.setCommand("#{inicioSesion.cerrarSesion()}");
        salir.setTitle("Cerrar la sesión actual");
        model.addElement(salir);

        Ajax.update("menuLateral");

    }


    public void iniciarSesion() {
        /*usuario = business.iniciar(nombreUsuario, password);
        if (usuario != null) {
            mostrarMensaje("Bienvenido de nuevo " + usuario.getNombre());
            init();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (Exception e) {
            }
        } else {
            mostrarMensaje("Usuario o contraseña incorrecta");
            usuario = new Usuario2();
        }*/
    }

    /*public void cerrarSesion() {
        usuario = null;
        init();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("inicioSesion.xhtml");
        } catch (Exception e) {
        }
    }*/

    public void mostrarMensaje(String mensaje) {
        FacesContext.getCurrentInstance()
                .addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Atencion", mensaje)
                );
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public BaseMenuModel getModel() {
        return model;
    }

    public void setModel(BaseMenuModel model) {
        this.model = model;
    }

    public OmegaMenuRenderer getRenderer() {
        return renderer;
    }

    public void setRenderer(OmegaMenuRenderer renderer) {
        this.renderer = renderer;
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

}
