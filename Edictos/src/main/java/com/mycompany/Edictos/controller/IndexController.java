/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Edictos.controller;

import com.mycompany.Edictos.business.UsuarioBusiness;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Admin
 */
@Named(value = "indexC")
@ViewScoped
public class IndexController implements Serializable {

    //Este es para el business para la base de datos
    @Inject
    UsuarioBusiness usuarioBusiness;
    
    //Lista de cosas
    
    /**
     * Creates a new instance of InicioSesion
     */
    public IndexController() {
    }
    
    @PostConstruct
    public void init(){
    }
    
    
    public void mostrarMensaje(String mensaje){
        FacesContext.getCurrentInstance()
                .addMessage(
                        null, 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Atencion", mensaje)
                );
    }
    
}
