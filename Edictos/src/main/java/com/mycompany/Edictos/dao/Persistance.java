/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Edictos.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Admin
 */
@Stateless
public class Persistance {
    @PersistenceContext(unitName = "com.mycompany.fany.Edictos")
    private EntityManager entityManager;
    
    public EntityManager getMng(){
        return entityManager;
    }
}
