/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Edictos.filter;

import static java.lang.Math.asin;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 *
 * @author iKhalil
 */
public class Extensiones {

    /**
     *
     * @param cadena Cadena a codificar
     * @return cadena codificada
     */
    public static String codificarABase64(String cadena) {
        // Encode data on your side using BASE64
        byte[] bytesEncoded = Base64.getEncoder().encode(cadena.getBytes());
        return new String(bytesEncoded);
    }

    /**
     *
     * @param cadena Cadena a codificar
     * @return cadena decodificada
     */
    public static String decodificarDeBase64(String cadena) {
        // Decode data on other side, by processing encoded data
        byte[] valueDecoded = Base64.getDecoder().decode(cadena);
        return new String(valueDecoded);
    }

    /**
     * Devuelve 4 puntos para comparar en una consulta SQL deacuerdo con el
     * radio dado
     *
     * @param latitud Latitud del punto a buscar
     * @param longitud Longitud del punto a buscar
     * @param radio radio que deben cubrir los 4 puntos
     *
     * @return [maxLat, minLat, maxLon, minLon]
     *
     * @throws NumberFormatException si latitud, longitud o radio son inválidos
     *
     */
    public static List<Double> crearGeocerca(String latitud, String longitud, String radio) throws NumberFormatException {

        List<Double> cerca = new ArrayList<>();

        double R = 6371.0; //Radio promedio de la tierra
        double x;
        double y;
        double rad;
        try {
            x = Float.parseFloat(latitud);
            y = Float.parseFloat(longitud);
            rad = Float.parseFloat(radio);
        } catch (NumberFormatException e) {
            throw e;
        }

        double maxLat = x + Math.toDegrees(rad / R);
        double minLat = x - Math.toDegrees(rad / R);
        double maxLon = y + Math.toDegrees(asin(rad / R) / Math.cos(Math.toRadians(x)));
        double minLon = y - Math.toDegrees(asin(rad / R) / Math.cos(Math.toRadians(x)));

        cerca.add(maxLat);
        cerca.add(minLat);
        cerca.add(maxLon);
        cerca.add(minLon);

        return cerca;
    }

    /**
     *
     * Función que ayuda a sacar la distancia entre 2 coordenadas cartesianas
     * (es decir con el formato (00.000000,00.0000000)) en metros en línea recta
     *
     * @param lat1 latitud del punto 1
     * @param lon1 longitud del punto 1
     * @param lat2 latitud del punto 2
     * @param lon2 longitud del punto 2
     * @return la distancia aproximada en metros en línea recta
     */
    public static double calcularDistanciaEntre2Puntos(double lat1, double lon1, double lat2, double lon2) {

        double d;
        double R = 6371.0; //Radio promedio de la tierra

        double latDistance = Math.toRadians(lat2) - Math.toRadians(lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        d = R * c * 1000; // convertiar a metros
        return d;
    }

    /**
     *
     * Función que ayuda a sacar la distancia entre 2 coordenadas cartesianas
     * (es decir con el formato (00.000000,00.0000000)) en metros en línea recta
     * recibiendo en cadena, si las cadenas son inválidas retorna 0.0 metros
     *
     * @param lat1 latitud del punto 1
     * @param lon1 longitud del punto 1
     * @param lat2 latitud del punto 2
     * @param lon2 longitud del punto 2
     * @return la distancia aproximada en metros en línea recta
     */
    public static double calcularDistanciaEntre2Puntos(String lat1, String lon1, String lat2, String lon2) {
        double latD1;
        double lonD1;
        double latD2;
        double lonD2;

        try {
            latD1 = Double.parseDouble(lat1);
            lonD1 = Double.parseDouble(lon1);
            latD2 = Double.parseDouble(lat2);
            lonD2 = Double.parseDouble(lon2);
            return Extensiones.calcularDistanciaEntre2Puntos(latD1, lonD1, latD2, lonD2);
        } catch (NumberFormatException | NullPointerException e) {
            return 0.0;
        }
    }
}
