package com.mycompany.Edictos.filter;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ViewExpiredException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class ApplicationFilter implements Filter {

    //private static final String FACES_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";
    
    private static final Logger LOG = Logger.getLogger(ApplicationFilter.class.getName());
    
    public ApplicationFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        //doBeforeProcessing
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        //doAfterProcessing
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doBeforeProcessing(request, response);

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        /*res.setHeader("X-XSS-Protection", "1; mode=block");
        res.addHeader("X-FRAME-OPTIONS", "DENY");
        res.addHeader("X-Content-Type-Options", "nosniff");
        res.addHeader("Strict-Transport-Security", "max-age=31536000");
        res.addHeader("Pragma", "no-cache");
        res.addHeader("Cache-Control", "no-cache");
        res.addHeader("Cache-Control", "must-revalidate");*/

        //Variables de ruta
        final String inicioSesion = "login.xhtml";
        final String index = "index.xhtml";

        try {
            
            //TMP
            if (true) {
                chain.doFilter(request, response);
                return;
            }
            
            //Recurso abierto
            if (req.getRequestURI().startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)
                    || req.getRequestURI().startsWith(req.getContextPath() + "/resources/")
                    || req.getRequestURL().toString().endsWith("404.xhtml")
                    || req.getRequestURL().toString().endsWith("error.xhtml")) {
                chain.doFilter(request, response);
                return;
            }

            //Sesión no  iniciada
            /*
            if (sesionController.getView().getUsuarioSesion() == null) {

                int cookies = 0;
                try {
                    cookies = getCookieValue(req);
                } catch (NumberFormatException e) {
                    LOG.log(Level.WARNING, "La cookie obtenida es inválida");
                    cookies = 0;
                }

                //Cookie válida, recuperando sesión
                if (cookies != 0) {
                    Usuarios usuario = usuarioBusiness.consultar(cookies);
                    if (usuario != null) {
                        sesionController.getView().setUsuarioSesion(usuario);
                        res.sendRedirect(req.getContextPath() + "/" + inicio);
                        return;
                    }
                }

                if (req.getRequestURL().toString().endsWith(inicioSesion)
                        || req.getRequestURL().toString().endsWith(registroUsuario)
                        || req.getRequestURL().toString().endsWith(registroFranquicias)
                        || req.getRequestURL().toString().contains("recuperarContrasena")
                        || req.getRequestURL().toString().contains("registroUsuario")
                        || req.getRequestURL().toString().endsWith("404.xhtml")
                        || req.getRequestURL().toString().endsWith("informacionRestaurantes.xhtml")
                        || req.getRequestURL().toString().endsWith("modificarContrasena.xhtml")
                        || req.getRequestURL().toString().endsWith("error.xhtml")
                        || req.getRequestURL().toString().endsWith("index.xhtml")
                        || req.getRequestURL().toString().endsWith("funcionales/franquicias.xhtml")
                        || req.getRequestURL().toString().endsWith("funcionales/restaurante.xhtml")
                        || req.getRequestURL().toString().endsWith("funcionales/servicioSeleccionado.xhtml")) { //Permitidas sin sesión

                    chain.doFilter(request, response);
                    return;
                } else if (req.getRequestURL().toString().contains("/modificarContrasena")) {
                    request.getParameter(URLDecoder.decode("user", "UTF-8"));
                    chain.doFilter(request, response);
                    return;
                } else {
                    res.sendRedirect(req.getContextPath() + "/index.xhtml");
                    return;
                }
            } else {
                if (sesionController.getView().getUsuarioSesion().getIdCatPerfilUsuario().getNombre().toUpperCase().equals("SUPERADMINISTRADOR")) {
                    chain.doFilter(request, response);
                    return;
                }
                if (sesionController.getView().getUsuarioSesion().getIdCatPerfilUsuario().getNombre().toUpperCase().equals("USUARIO")) {
                    if (validarSitiosUsuario(req)) {
                        chain.doFilter(request, response);
                        return;
                    } else {
                        res.sendRedirect(req.getContextPath() + "/webapp/funcionales/franquicias.xhtml");
                        return;
                    }
                }
                if (sesionController.getView().getUsuarioSesion().getIdCatPerfilUsuario().getNombre().toUpperCase().equals("ADMINISTRADORFRANQUICIAS")
                        || sesionController.getView().getUsuarioSesion().getIdCatPerfilUsuario().getNombre().equals("ADMINISTRADORRESTAURANTE")) {
                    if (validarSitiosUsuario(req) || validarSitiosAdminFranRest(req)) {
                        chain.doFilter(request, response);
                        return;
                    } else {
                         res.sendRedirect(req.getContextPath() + "/webapp/funcionales/franquicias.xhtml");
                        return;
                    }
                }
                if (req.getRequestURL().toString().endsWith(inicioSesion)
                        || req.getRequestURL().toString().endsWith(registroUsuario)
                        || req.getRequestURL().toString().endsWith(registroFranquicias)
                        || req.getRequestURL().toString().endsWith("recuperarContrasena.xhtml")
                        || req.getRequestURL().toString().endsWith("modificarContrasena.xhtml")) {
                    res.sendRedirect(req.getContextPath() + "/webapp/funcionales/franquicias.xhtml");
                    return;
                } else {
                    chain.doFilter(request, response);
                    return;
                }
            }*/
        } catch (ViewExpiredException e) {
            res.sendRedirect(req.getContextPath() + "/" + index);
            return;
        }
    }

    
    public int getCookieValue(HttpServletRequest request) throws NumberFormatException {
        String valor = CookieManager.obtenerValorCookie(request, "recordarSesion");
        if (valor == null) {
            return 0;
        }
        try {
            return Integer.parseInt(valor);
        } catch (NumberFormatException e) {
            throw e;
        }
    }

    @Override
    public void destroy() {

    }   
}
