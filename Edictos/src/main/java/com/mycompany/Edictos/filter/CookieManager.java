/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Edictos.filter;

import javax.ejb.Stateless;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author iKhalil
 */
@Stateless
public class CookieManager {

    public static void agregarCookie(HttpServletResponse respuesta, String nombre, String valor, int expiracion) {
        Cookie cookie = new Cookie(nombre, Extensiones.codificarABase64(valor));
        cookie.setMaxAge(expiracion);
        
        /*
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        */
        respuesta.addCookie(cookie);
    }

    public static Cookie obtenerCookie(HttpServletRequest peticion, String nombre){
        Cookie[] cookies = peticion.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (nombre.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }
        return null;
    }
    
    public static String obtenerValorCookie(HttpServletRequest peticion, String nombre) {
        Cookie cookie = obtenerCookie(peticion, nombre);
        if (cookie == null) {
            return null;
        } else {
            return Extensiones.decodificarDeBase64(cookie.getValue());
        }
    }
    
    public static String obtenerValorCookie(Cookie cookie) {
        if (cookie == null) {
            return null;
        } else {
            return Extensiones.decodificarDeBase64(cookie.getValue());
        }
    }

}

