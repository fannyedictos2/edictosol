var status = "";
//window.onload = function () {
//    if (navigator.geolocation) {
//        console.log("Si cuenta con soporte");
//        navigator.geolocation.getCurrentPosition(obtenerPosicion, erroresLocalizacion);
//    } else {
//        console.log("No cuenta con soporte");
//    }
//};

function obtenerPosicion(posicion) {
    var currentMarker = null;
    var oldMarker = null;

    if (currentMarker === null) {
        var lat = posicion.coords.latitude;
        var lng = posicion.coords.longitude;
        document.getElementById('lat').value = lat;
        document.getElementById('lng').value = lng;

        currentMarker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng)
        });

        PF('map').addOverlay(currentMarker);
        PF('dlg').show();
    }

}

function erroresLocalizacion(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            console.log("No hay acceso por parte de esta página");
            document.getElementById('lat').value = null;
            document.getElementById('lng').value = null;
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Estás en narnia");
            document.getElementById('lat').value = null;
            document.getElementById('lng').value = null;
            break;
        case error.TIMEOUT:
            console.log("Te están robando el internet");
            document.getElementById('lat').value = null;
            document.getElementById('lng').value = null;
            break;
        case error.UNKNOWN_ERROR:
            console.log("Te están robando el internet");

            break;
    }
}

function obtenerPosicion2(posicion) {
    var currentMarker = null;
    var oldMarker = null;

    if (currentMarker === null) {
        var lat = posicion.coords.latitude;
        var lng = posicion.coords.longitude;
        document.getElementById('lat2').value = lat;
        document.getElementById('lng2').value = lng;

        currentMarker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng)
        });

        PF('mapMapaBar').addOverlay(currentMarker);
        PF('dlg2').show();
    }

}