/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Translate address to coordinates for beans
function geocode() {

    var map = PF('map').getMap();//PERFECT

    var address = document.getElementById('address').value;//PERFECT

    var geocoder = new google.maps.Geocoder();//PERFECT

    geocoder.geocode({'address': address}, function (results, status) {

        if (status === 'OK') {
            PF('map').geocode(address);
        } else {
            PF('alertDLG').show();
            PF('blockBuscar').unblock();
        }
    });
}

//Translate address to coordinates for beans
function geocodeMapaBar() {

    var map = PF('mapMapaBar').getMap();//PERFECT

    var addressMapaBar = document.getElementById('addressMapaBar').value;//PERFECT

    var geocoder = new google.maps.Geocoder();//PERFECT

    console.log('geocoding');
    geocoder.geocode({'address': addressMapaBar}, function (results, status) {

        console.log('done');
        if (status === 'OK') {
            PF('mapMapaBar').geocode(addressMapaBar);
        } else {
            PF('alertDLGMapaBar').show();
            PF('blockBuscarMapaBar').unblock();
        }
    });
}

function reverseGeocode() {
    console.log("Reverse mode map");
    var lat = document.getElementById('formLoc:lat').value, lng = document.getElementById('formLoc:lng').value;
    PF('revGeoMap').reverseGeocode(lat, lng);
}
