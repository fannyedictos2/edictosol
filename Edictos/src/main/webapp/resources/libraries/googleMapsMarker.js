/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Get marker coordinates and put it onto the map
function handlePointClick(event) {
    //Obtener datos
    var lat = event.latLng.lat();
    document.getElementById('lat').value = lat;
    var lng = event.latLng.lng();
    document.getElementById('lng').value = lng;

    PF('dlg').show();
}

//Complete and send marker coordinates
function markerAddComplete() {
    PF('dlg').hide();
    return true;
}

//Cancel add new marker
function cancel() {
    PF('dlg').hide();
    
    document.getElementById('lat').value = 0.0;
    document.getElementById('lng').value = 0.0;

    return false;
}
//Cancel add new marker
function cancelmapa() {
    PF('dlg').hide();
    
    document.getElementById('lat').value = 0.0;
    document.getElementById('lng').value = 0.0;

    return false;
}






function handlePointClick2(event) {
    //Obtener datos
    var lat = event.latLng.lat();
    document.getElementById('lat2').value = lat;
    var lng = event.latLng.lng();
    document.getElementById('lng2').value = lng;

    PF('dlg2').show();
}

//Complete and send marker coordinates
function markerAddComplete2() {
    PF('dlg2').hide();
    return true;
}

//Cancel add new marker
function cancel2() {
    PF('dlg2').hide();
    
    document.getElementById('lat2').value = 0.0;
    document.getElementById('lng2').value = 0.0;

    return false;
}
